# Ansible role PHP

Simple Ansible role to install and configure PHP in my self-hosting context

The optional following variables can be set to customize the PHP configuration.
Example:
```
    vars:
      php_memory_limit: 512M
      php_post_max_size: 250M
      php_upload_max_filesize: 200M
```